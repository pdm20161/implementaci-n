package pe.edu.uni.pdm_20161.arbolgenealogico;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
   static final String appId = "4B97FDD1-4070-4157-BF08-C1EF8D2BED31";
   static String userID = "";
   static String nickname = "";
   static int ID = -1;


   /*ASYNCTASK*/
   ProgressDialog pDialog;
   private static StaticVariables staticVariables = new StaticVariables();
   JSONParser jParser = new JSONParser();
   private static String url_login = "http://" + staticVariables.getIP() + "/get_user_and_name.php";
   private static final String TAG_SUCCESS = staticVariables.getTagSuccess();
   /**/

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);

      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      Bundle extras = getIntent().getExtras();
      if(ID==-1) {
         ID = extras.getInt("id_usuario");
      }

      new obtenerUserAndName().execute();
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_main, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      //noinspection SimplifiableIfStatement
      if (id == R.id.action_profile) {
         Intent intent = new Intent(MainActivity.this, PerfilActivity.class);
         startActivity(intent);
      }

      return super.onOptionsItemSelected(item);
   }

   public void sendMaps(View view) {
      //Intent intent = new Intent(MainActivity.this, MapsActivity.class);
      //startActivity(intent);
      startActivity(new Intent(this, MapsActivity.class));
      overridePendingTransition(R.anim.right_in, R.anim.right_out);
   }

   public void sendSalaChat(View view) {
      //Intent intent = new Intent(MainActivity.this, SalaChatActivity.class);
      //startActivity(intent);

      Intent intent;
      if (!userID.equals("") || !nickname.equals("")) {
         intent = new Intent(this, SalaChatActivity.class);
         intent.putExtra("AppID", appId);
         intent.putExtra("UserID", userID);
         intent.putExtra("Nickname", nickname);
         startActivity(intent);
      } else {
         intent = new Intent(this, ChatIntro.class);
         intent.putExtra("AppID", appId);
         startActivity(intent);
      }
      overridePendingTransition(R.anim.left_in, R.anim.left_out);
   }

   public boolean onKeyDown(int keyCode, KeyEvent event){
      if (keyCode == KeyEvent.KEYCODE_BACK) {

         new AlertDialog.Builder(this)
                 .setIcon(android.R.drawable.ic_dialog_alert)
                 .setTitle("Salir")
                 .setMessage("Estás seguro?")
                 .setNegativeButton(android.R.string.cancel, null)//sin listener
                 .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener(){//un listener que al pulsar, cierre la aplicacion
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                       //Salir
                       MainActivity.this.finish();
                       salir();
                    }

                 })
                 .show();

         return true;
      }

      return true;
   }

   public void salir(){
      finish();
      //startActivity(new Intent(this, LoginActivity.class));
      overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
   }





/*----------hilos -------------*/

   /*ASYNC TASK*/
   public class obtenerUserAndName extends AsyncTask<String, String, String>{
      int idU;

      public obtenerUserAndName(){}

      @Override
      protected void onPreExecute() {
         super.onPreExecute();
         pDialog = new ProgressDialog(MainActivity.this);
         pDialog.setMessage("Obteniendo datos de usuario...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(false);
         pDialog.show();
      }

      @Override
      protected String doInBackground(String... values) {
         List<NameValuePair> params = new ArrayList<NameValuePair>();
         params.add(new BasicNameValuePair("id", ""+ID));
         Log.e("IDDDDDDDDDd", ""+ID);
         JSONObject json = jParser.makeHttpRequest(url_login, "POST", params);


         try {
            int success = json.getInt(TAG_SUCCESS);

            Log.e("Mensaje id:", json.getString("message"));

            if (success == 1) {
               userID = json.getString("user");
               nickname = json.getString("name");
               return "Correcto";
            }
         } catch (JSONException e) {
            e.printStackTrace();
         }

         return "";
      }

      protected void onPostExecute(String params){
         pDialog.dismiss();
         if(!params.equals("Correcto")){
            Toast.makeText(MainActivity.this, "Error al obtener datos de usuario.",
                    Toast.LENGTH_LONG).show();
         }
      }


   }


}
