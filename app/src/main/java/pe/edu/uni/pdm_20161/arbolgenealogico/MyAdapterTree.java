package pe.edu.uni.pdm_20161.arbolgenealogico;

import android.content.Context;
import android.graphics.PixelFormat;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;

import com.google.android.gms.vision.Frame;

import java.util.List;

/**
 * Created by noa on 15/02/16.
 */
public class MyAdapterTree extends ArrayAdapter<SurfaceView> {

    private Context context;
    private SurfaceView[] surfaceViews;

    public MyAdapterTree(Context context, SurfaceView[] surfaceViews){
        super(context, R.layout.item_arbol_genealogico, surfaceViews);
        this.context = context;
        this.surfaceViews = surfaceViews;
    }

    @Override
    public View getView(int ind, View view, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //Obtiene campos de la vista
        View _view = inflater.inflate(R.layout.item_arbol_genealogico,parent,false);
        SurfaceView surfaceVView= (SurfaceView) _view.findViewById(R.id.surfaceView);

        //Seteamos los valores
        surfaceVView = this.surfaceViews[ind];

        return _view;
    }

}
