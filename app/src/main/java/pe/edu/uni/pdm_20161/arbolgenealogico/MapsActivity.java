package pe.edu.uni.pdm_20161.arbolgenealogico;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.search.SearchAuthApi;

import org.xml.sax.helpers.LocatorImpl;

import java.security.Security;
import java.util.ArrayList;
import java.util.HashMap;

import pe.edu.uni.pdm_20161.arbolgenealogico.Map.MyMarker;

public class MapsActivity extends AppCompatActivity {

   private GoogleMap mMap; // Might be null if Google Play services APK is not available.
   private HashMap<Marker, MyMarker> mMarkersHashMap;
   private ArrayList<MyMarker> mMyMarkersArrayList;
   protected double myLongitud, myLatitude;
   protected Location user;
   protected float distancia;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_maps);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      setMarkers();
      setUpMapIfNeeded();
      plotMarkers(mMyMarkersArrayList);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_chat, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      //noinspection SimplifiableIfStatement
      if (id == R.id.action_settings) {
         return true;
      }

      if (id == android.R.id.home) {
         startActivity(new Intent(this, MainActivity.class));
         overridePendingTransition(R.anim.left_in, R.anim.left_out);

         return true;
      }

      return super.onOptionsItemSelected(item);
   }

   @Override
   protected void onResume() {
      super.onResume();
      //setMarkers();
      setUpMapIfNeeded();
      //plotMarkers(mMyMarkersArrayList);
   }

   /**
    * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
    * installed) and the map has not already been instantiated.. This will ensure that we only ever
    * call {@link #setUpMap()} once when {@link #mMap} is not null.
    * <p/>
    * If it isn't installed {@link SupportMapFragment} (and
    * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
    * install/update the Google Play services APK on their device.
    * <p/>
    * A user can return to this FragmentActivity after following the prompt and correctly
    * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
    * have been completely destroyed during this process (it is likely that it would only be
    * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
    * method in {@link #onResume()} to guarantee that it will be called.
    */
   private void setUpMapIfNeeded() {
      // Do a null check to confirm that we have not already instantiated the map.
      if (mMap == null) {
         // Try to obtain the map from the SupportMapFragment.
         mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
               .getMap();
         // Check if we were successful in obtaining the map.
         if (mMap != null) {
            setUpMap();
         }
      }
   }

   private void setMarkers() {
      mMyMarkersArrayList = new ArrayList<MyMarker>();
      mMarkersHashMap = new HashMap<Marker, MyMarker>();

      mMyMarkersArrayList.add(
            new MyMarker("Juan Carlos\nCarbajal Ipenza",
                  "icon1",
                  Double.parseDouble("-12.0711"),
                  Double.parseDouble("-77.0534")));
      mMyMarkersArrayList.add(
            new MyMarker("Noemi Maritza\nLapa Romero",
                  "icon2",
                  Double.parseDouble("-12.0691"),
                  Double.parseDouble("-77.0514")));
      mMyMarkersArrayList.add(
            new MyMarker("David\nAparco Cardenas",
                  "icon3",
                  Double.parseDouble("-12.0711"),
                  Double.parseDouble("-77.0514")));
      mMyMarkersArrayList.add(
            new MyMarker("Henry Guido\nFelix Merino",
                  "icon4",
                  Double.parseDouble("-12.0691"),
                  Double.parseDouble("-77.0534")));

   }

   private void plotMarkers(ArrayList<MyMarker> markers) {
      if (markers.size() > 0 ) {
         for (MyMarker myMarker : markers) {
            MarkerOptions markerOptions = new MarkerOptions()
                  .position(new LatLng(myMarker.getmLatitude(), myMarker.getmLongitude()));
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            Marker currentMarker = mMap.addMarker(markerOptions);
            mMarkersHashMap.put(currentMarker, myMarker);
            mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
         }
      }
   }
   /**
    * This is where we can add markers or lines, add listeners or move the camera. In this case, we
    * just add a marker near Africa.
    * <p/>
    * This should only be called once and when we are sure that {@link #mMap} is not null.
    */
   private void setUpMap() {
      try {
         /*LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
         Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
         lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 10, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
               myLongitud = location.getLongitude();
               myLatitude = location.getLatitude();
               distancia = location.distanceTo(user);
               Toast.makeText(MapsActivity.this, distancia + "m", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
         });*/

         mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

         user = new Location("");
         user.setLatitude(-12.069622);
         user.setLongitude(-77.0449119);

         LatLng user1 = new LatLng(user.getLatitude(), user.getLongitude());
         CameraPosition camPos = new CameraPosition.Builder()
                 .target(user1)   //Centramos el mapa en Madrid
                 .zoom(15)         //Establecemos el zoom en 19
                 .bearing(45)      //Establecemos la orientación con el noreste arriba
                 .tilt(0)         //Bajamos el punto de vista de la cámara 70 grados
                 .build();

         CameraUpdate camUpd3 =
                 CameraUpdateFactory.newCameraPosition(camPos);

         mMap.animateCamera(camUpd3);
         if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                 != PackageManager.PERMISSION_GRANTED
                 && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                 != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
         }
         mMap.setMyLocationEnabled(true);
         mMap.getUiSettings().setZoomControlsEnabled(true);
         mMap.getUiSettings().setMyLocationButtonEnabled(true);
         mMap.getUiSettings().setAllGesturesEnabled(true);

         mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
               marker.showInfoWindow();
               return true;
            }
         });

         //myLongitud = location.getLongitude();
         //myLatitude = location.getLatitude();

         //distancia = location.distanceTo(user);
         //Toast.makeText(MapsActivity.this, distancia+"m", Toast.LENGTH_LONG).show();

      } catch (SecurityException se) {}



   }

   public class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
      public MarkerInfoWindowAdapter() {
      }

      @Override
      public View getInfoWindow(Marker marker) {
         return null;
      }

      @Override
      public View getInfoContents(Marker marker) {
         View view = getLayoutInflater().inflate(R.layout.infowindow, null);
         MyMarker myMarker = mMarkersHashMap.get(marker);
         ImageView markerIcon = (ImageView) view.findViewById(R.id.marker_icon);
         TextView markerLabel = (TextView) view.findViewById(R.id.marker_label);
         markerIcon.setImageResource(manageMarkerIcon(myMarker.getmIcon()));
         markerLabel.setText(myMarker.getmLabel());
         return view;
      }

      private int manageMarkerIcon(String markerIcon) {
         if (markerIcon.equals("icon1"))
            return R.drawable.juan;
         else if (markerIcon.equals("icon2"))
            return R.drawable.maritza;
         else if (markerIcon.equals("icon3"))
            return R.drawable.david;
         else if (markerIcon.equals("icon4"))
            return R.drawable.guido;
         else
            return R.mipmap.ic_nouser;
      }
   }

   public boolean onKeyDown(int keyCode, KeyEvent event){
      if (keyCode == KeyEvent.KEYCODE_BACK) {
         //startActivity(new Intent(this, MainActivity.class));
         finish();
         overridePendingTransition(R.anim.left_in, R.anim.left_out);

         return true;
      }

      return true;
   }
}
