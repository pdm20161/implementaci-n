package pe.edu.uni.pdm_20161.arbolgenealogico;

/**
 * Created by noa on 08/03/16.
 */
public class StaticVariables {
    public static String ip_server = "daparcoc.pe.hu/arbol_genealogico";
    public static String TAG_SUCCESS = "success";

    public String getIP(){
        return ip_server;
    }

    public String getTagSuccess(){
        return TAG_SUCCESS;
    }

}

