package pe.edu.uni.pdm_20161.arbolgenealogico;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.UserListQuery;
import com.sendbird.android.model.User;

public class SalaChatActivity extends AppCompatActivity implements OnItemClickListener {

    String[] member_names;
    TypedArray profile_pics;
    String[] lastView;
    List<User> mUsers;
    List<RowItem> rowItems;
    ListView mylistview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sala_chat);

        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.verde_bosque)));

        mylistview = (ListView) findViewById(R.id.list);

        initSendBird(getIntent().getExtras());

        mylistview.setOnItemClickListener(this);
        mylistview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(SalaChatActivity.this, PerfilActivity.class);
                startActivity(intent);
                return true;
            }
        });

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        String member_name = rowItems.get(position).getMember_name();
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("pNickname", mUsers.get(position).getName());
        intent.putExtra("pUserID", mUsers.get(position).getId());
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sala_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        /*if (id == R.id.action_settings) {
            return true;
        }*/

        if (id == android.R.id.home) {
            startActivity(new Intent(this, MainActivity.class));
            overridePendingTransition(R.anim.right_in, R.anim.right_out);

            return true;
        }

        if (id == R.id.action_update) {
            loadUsers();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public boolean onKeyDown(int keyCode, KeyEvent event){
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //startActivity(new Intent(this, MainActivity.class));
            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            return true;
        }

        return true;
    }

    private void initSendBird(Bundle extras) {
        String appKey = MainActivity.appId;//extras.getString("AppID");
        String userID = MainActivity.userID;//extras.getString("UserID");
        String nickname = MainActivity.nickname;//extras.getString("Nickname");

        SendBird.init(appKey);
        SendBird.login(userID, nickname);

        loadUsers();
    }

    private void loadUsers() {
        rowItems = new ArrayList<RowItem>();
        UserListQuery mUserListQuery = SendBird.queryUserList();
        mUserListQuery.setLimit(30);
        if(mUserListQuery != null && mUserListQuery.hasNext() && !mUserListQuery.isLoading()) {
            mUserListQuery.next(new UserListQuery.UserListQueryResult() {
                @Override
                public void onResult(List<User> users) {
                    for (int i = 0; i < users.size(); ++i) {
                        RowItem item = new RowItem(users.get(i).getName(),
                                R.drawable.ic_male,
                                users.get(i).getId());
                        rowItems.add(item);
                    }
                    CustomAdapter adapter = new CustomAdapter(SalaChatActivity.this, rowItems);
                    mylistview.setAdapter(adapter);
                    mUsers = users;
                }

                @Override
                public void onError(SendBirdException e) {
                }
            });
        }
    }

}