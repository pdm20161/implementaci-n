package pe.edu.uni.pdm_20161.arbolgenealogico;

import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class LocalizarFamiliarActivity extends AppCompatActivity {

   private ListView lv;
   private SurfaceView [] mySurfaceViews = new SurfaceView[6];
   private String[] datos = new String[]{
         "Carbajal Ipenza",
         "Lapa Romero",
         "Aparco Cardenas",
         "Felix Merino"
   };

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_localizar_familiar);

      AdaptadorFamilias adaptador = new AdaptadorFamilias(this, datos);
      lv = (ListView)findViewById(R.id.listViewTrees);

      //------------ACTVIDAD 2-------------------Descomentar para ver:
      lv.setAdapter(adaptador);

   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_localizar_familiar, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      //noinspection SimplifiableIfStatement
      if (id == R.id.action_settings) {
         return true;
      }

      return super.onOptionsItemSelected(item);
   }

   public void goToLogin(View view){
      //Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
      //startActivity(intent);
      startActivity(new Intent(this, LoginActivity.class));
      overridePendingTransition(R.anim.zoom_forward_in,
              R.anim.zoom_forward_out);
   }

   class AdaptadorFamilias extends ArrayAdapter<String> {
      public AdaptadorFamilias(Context context, String[] datos) {
         super(context, R.layout.item_arbol_genealogico, datos);
      }

      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
         LayoutInflater inflater = LayoutInflater.from(getContext());
         View item = inflater.inflate(R.layout.item_arbol_genealogico, null);

         TextView nombreFamilia = (TextView) item.findViewById(R.id.nombreFamilia);
         nombreFamilia.setText(datos[position]);
         return (item);
      }
   }
}
