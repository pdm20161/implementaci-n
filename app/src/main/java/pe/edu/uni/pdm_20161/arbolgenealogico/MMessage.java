package pe.edu.uni.pdm_20161.arbolgenealogico;

import com.sendbird.android.SendBird;
import com.sendbird.android.model.MessageModel;
import com.sendbird.android.shadow.com.google.gson.Gson;
import com.sendbird.android.shadow.com.google.gson.JsonElement;
import com.sendbird.android.shadow.com.google.gson.JsonObject;

public class MMessage extends MessageModel {
    private String fromName;
    private boolean isSelf;
    private String message = "";
    private MMessage.Sender sender;
    private boolean isOpMessage;
    private boolean isGuestMessage;
    private String data = "";
    private JsonObject jsonObj;
    private String tempId;

    public MMessage() {
    }

    public MMessage(String fromName, String message, boolean isSelf) {
        this.message = message;
        this.isSelf = isSelf;
        this.fromName = fromName;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSelf() {
        return isSelf;
    }

    public void setSelf(boolean isSelf) {
        this.isSelf = isSelf;
    }

    public static MMessage build(JsonElement el) {
        return build(el, true);
    }

    public static MMessage build(JsonElement el, boolean present) {
        MMessage m = new MMessage();

        try {
            JsonObject e = el.getAsJsonObject();
            m.setPresent(present);
            m.parseMessageId(e);
            m.parseMessageTimestamp(e);
            m.jsonObj = e;
            m.message = e.get("message").getAsString();
            m.isOpMessage = e.get("is_op_msg").getAsBoolean();
            m.isGuestMessage = e.get("is_guest_msg").getAsBoolean();
            m.sender = MMessage.Sender.build(e.get("user"));
            m.data = e.has("data")?e.get("data").getAsString():"";
            m.tempId = e.has("tid")?e.get("tid").getAsString():"";
            return m;
        } catch (Exception var4) {
            var4.printStackTrace();
            return null;
        }
    }

    public String toJson() {
        return (new Gson()).toJson(this.jsonObj);
    }

    public JsonElement toJsonElement() {
        return this.jsonObj;
    }

    public boolean isBlocked() {
        return SendBird.isUserBlocked(this.sender.getId(), this.getTimestamp());
    }

    /** @deprecated */
    @Deprecated
    public boolean hasSameSender(MMessage msg) {
        return this.sender.getId() == msg.sender.getId();
    }

    /** @deprecated */
    @Deprecated
    public void mergeWith(MMessage mergee) {
        this.message = this.message + "\n" + mergee.getMessage();
    }

    public String getMessage() {
        return this.message;
    }

    public String getData() {
        return this.data;
    }

    public String getTempId() {
        return this.tempId;
    }

    public String getSenderName() {
        return this.sender == null?"":this.sender.getName();
    }

    public String getSenderImageUrl() {
        return this.sender == null?"":this.sender.getImageUrl();
    }

    public String getSenderId() {
        return this.sender == null?"":this.sender.getGuestId();
    }

    /** @deprecated */
    @Deprecated
    public boolean isOpMessage() {
        return this.isOpMessage;
    }

    private static class Sender {
        private long id = -1L;
        private String name = "";
        private String imageUrl = "";
        private String guestId = "";

        public static MMessage.Sender build(JsonElement el) throws Exception {
            MMessage.Sender s = new MMessage.Sender();

            try {
                JsonObject e = el.getAsJsonObject();
                s.id = e.get("id").getAsLong();
                s.name = e.get("name").getAsString();
                s.imageUrl = e.get("image").getAsString();
                s.guestId = e.has("guest_id")?e.get("guest_id").getAsString():"";
                return s;
            } catch (Exception var3) {
                var3.printStackTrace();
                throw var3;
            }
        }

        protected Sender() {
        }

        public long getId() {
            return this.id;
        }

        public String getName() {
            return this.name;
        }

        public String getImageUrl() {
            return this.imageUrl;
        }

        public String getGuestId() {
            return this.guestId;
        }
    }

}