package pe.edu.uni.pdm_20161.arbolgenealogico;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.ads.formats.NativeAd;

import android.util.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {

   private static StaticVariables staticVariables = new StaticVariables();

   // Creating JSON Parser object
   /*JSON LOGIN*/
   JSONParser jParser = new JSONParser();
   private static String url_all_countries = "http://" + staticVariables.getIP() + "/get_all_countries.php";
   JSONArray paises = null;
   private ProgressDialog pDialog;
   List<String> spinnerNationality;
   /**/

   EditText editTextNombre;
   EditText editTextApaterno;
   EditText editTextAmaterno;
   EditText editTextUsuario;
   EditText editTextEmail;
   EditText editTextContrasenha;
   EditText editTextConfirmarContrasenha;
   Spinner spinnerNacionalidad;
   EditText editTextFechaNacimiento;
   RadioButton radioButtonGeneroMasculino;
   RadioButton radioButtonGeneroFemenino;
   ImageView imageView;
   Bitmap bitmap;
   ProgressDialog dialog;

   String str_nombre, str_apaterno, str_amaterno, str_usuario, str_email, str_contrasenha, str_confirmar_contrasenha;
   String str_fecha_nacimiento, str_pais, str_genero, ruta_foto;


   /**/
   Button btpic, btnup;
   private Uri fileUri;
   Uri selectedImage;
   Bitmap photo;
   String ba1;
   public static String URL = "http://" + staticVariables.getIP() + "/update_picture.php";
   /**/

   /*JSON REGISTER*/
   private static String url_register = "http://" + staticVariables.getIP() + "/register_user.php";
   JSONArray registerStatus = null;
    /**/


   private static final String TAG_SUCCESS = staticVariables.getTagSuccess();

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_register);

      editTextNombre = (EditText)findViewById(R.id.registerName);
      editTextApaterno = (EditText)findViewById(R.id.registerLastNameOne);
      editTextAmaterno = (EditText)findViewById(R.id.registerLastNameTwo);
      editTextUsuario = (EditText)findViewById(R.id.registerUser);
      editTextEmail = (EditText)findViewById(R.id.registerEmail);
      editTextContrasenha = (EditText)findViewById(R.id.registerPasswordOne);
      editTextConfirmarContrasenha = (EditText)findViewById(R.id.registerPasswordTwo);
      spinnerNacionalidad = (Spinner) findViewById(R.id.spinnerNationality);
      editTextFechaNacimiento = (EditText)findViewById(R.id.registerDate);
      radioButtonGeneroMasculino = (RadioButton)findViewById(R.id.radioMasculinoButton);
      radioButtonGeneroFemenino = (RadioButton)findViewById(R.id.radioFemaleButton);
      imageView = (ImageView)findViewById(R.id.imageView);

      new LoadAllCountries().execute();

      bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_nouser);
      ByteArrayOutputStream baos=new  ByteArrayOutputStream();
      bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
      byte [] b=baos.toByteArray();
      ba1 = Base64.encodeToString(b, Base64.DEFAULT);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_register, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      //noinspection SimplifiableIfStatement
      if (id == R.id.action_settings) {
         return true;
      }

      return super.onOptionsItemSelected(item);
   }

   public void sendRegister(View view){

      str_nombre = editTextNombre.getText().toString();
      str_apaterno = editTextApaterno.getText().toString();
      str_amaterno = editTextAmaterno.getText().toString();
      str_usuario = editTextUsuario.getText().toString();
      str_email = editTextEmail.getText().toString();
      str_contrasenha = editTextContrasenha.getText().toString();
      str_confirmar_contrasenha = editTextConfirmarContrasenha.getText().toString();
      str_fecha_nacimiento = editTextFechaNacimiento.getText().toString();
      str_pais = spinnerNacionalidad.getSelectedItem().toString();
      ruta_foto = System.currentTimeMillis() + ".png";

      if(radioButtonGeneroMasculino.isChecked()) str_genero = "M";
      if(radioButtonGeneroFemenino.isChecked()) str_genero = "F";

      new validarRegistro().execute();
   }

   public void choosePicture(View v){
      clickpic();
   }

   private void clickpic() {
      // Check Camera
      if (getApplicationContext().getPackageManager().hasSystemFeature(
              PackageManager.FEATURE_CAMERA)) {
         // Open default camera
         Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
         intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

         // start the image capture Intent
         startActivityForResult(intent, 100);

      } else {
         Toast.makeText(getApplication(), "Camera not supported", Toast.LENGTH_LONG).show();
      }
   }

   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      if (requestCode == 100 && resultCode == RESULT_OK) {

         selectedImage = data.getData();
         photo = (Bitmap) data.getExtras().get("data");

         // Cursor to get image uri to display

         String[] filePathColumn = {MediaStore.Images.Media.DATA};

         Bitmap photo = (Bitmap) data.getExtras().get("data");
         imageView = (ImageView) findViewById(R.id.imageView);
         imageView.setImageBitmap(photo);

         ByteArrayOutputStream baos=new  ByteArrayOutputStream();
         photo.compress(Bitmap.CompressFormat.PNG,100, baos);
         byte [] b=baos.toByteArray();

         ba1 = Base64.encodeToString(b, Base64.DEFAULT);
      }
   }

   public void deletePicture(View v){
      imageView.setImageResource(R.mipmap.ic_nouser);
      bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_nouser);
      ByteArrayOutputStream baos=new  ByteArrayOutputStream();
      bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
      byte [] b=baos.toByteArray();
      ba1 = Base64.encodeToString(b, Base64.DEFAULT);
   }

   public class LoadAllCountries extends AsyncTask<String, String, String> {

      @Override
      protected void onPreExecute() {
         super.onPreExecute();
         pDialog = new ProgressDialog(RegisterActivity.this);
         pDialog.setMessage("Cargando países...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(false);
         pDialog.show();
      }

      protected String doInBackground(String... args) {
         List<NameValuePair> params = new ArrayList<NameValuePair>();
         JSONObject json = jParser.makeHttpRequest(url_all_countries, "POST", params);

         //Log.d("All Countries: ", json.toString());

         try {
            int success = json.getInt(TAG_SUCCESS);

            spinnerNationality =  new ArrayList<String>();
            spinnerNationality.add("-SELECCIONE");

            if (success == 1) {
               paises = json.getJSONArray("paises");

               for (int i = 0; i < paises.length(); i++) {
                  JSONObject c = paises.getJSONObject(i);

                  String paisNombre = c.getString("pais");


                  spinnerNationality.add(paisNombre);

               }
            }
         } catch (JSONException e) {
            e.printStackTrace();
         }

         return null;
      }

      protected void onPostExecute(String file_url) {
         // dismiss the dialog after getting all products
         pDialog.dismiss();
         // updating UI from Background Thread
         runOnUiThread(new Runnable() {
            public void run() {
               ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegisterActivity.this,
                       android.R.layout.simple_spinner_dropdown_item, spinnerNationality);

               adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
               Spinner sItems = (Spinner) findViewById(R.id.spinnerNationality);
               sItems.setAdapter(adapter);

            }
         });

      }

   }

   public class validarRegistro extends AsyncTask<String, String, Integer> {

      public validarRegistro(){
      }

      public boolean isDateValid(String date){
         try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            df.setLenient(false);
            df.parse(date);
            return true;
         } catch (ParseException e) {
            return false;
         }
      }

      public boolean isEmailValid(String email) {
         boolean isValid = false;
         String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
         CharSequence inputStr = email;
         Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
         Matcher matcher = pattern.matcher(inputStr);
         if (matcher.matches()) {
            isValid = true;
         }
         return isValid;
      }

      public Integer validarDatos(){
         if(str_nombre.length()<1) return -1;
         if(str_apaterno.length()<1) return -2;
         if(str_amaterno.length()<1) return -3;
         if(str_usuario.length()<1) return -4;
         if(str_email.length()<1) return -12;
         if(!isEmailValid(str_email)) return -13; //formato de correo incorrecto;
         if(str_contrasenha.length()<6) return -5;
         if(str_confirmar_contrasenha.length()<6) return -6;
         if(!str_confirmar_contrasenha.equals(str_contrasenha)) return -7;
         if(str_pais.equals("-SELECCIONE")) return -8;
         if(str_fecha_nacimiento.length()!=10) return -9;
         if(!isDateValid(str_fecha_nacimiento)) return -10; //formato de fecha incorrecto;
         if(!radioButtonGeneroFemenino.isChecked() && !radioButtonGeneroMasculino.isChecked()) return -11;
         return 0;
      }

      public String md5(String s) {
         try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i=0; i<messageDigest.length; i++)
               hexString.append(Integer.toHexString(0xFF & messageDigest[i]));

            Log.d("pass", hexString.toString());

            return hexString.toString();

         } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
         }
         return "";
      }

      @Override
      protected void onPreExecute() {
         super.onPreExecute();
         pDialog = new ProgressDialog(RegisterActivity.this);
         pDialog.setMessage("Verificando usuario...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(false);
         pDialog.show();
      }

      @Override
      protected Integer doInBackground(String... args) {
         List<NameValuePair> params = new ArrayList<NameValuePair>();
         Integer resultado = validarDatos();

         if(resultado==0){
            params.add(new BasicNameValuePair("nombre", str_nombre));
            params.add(new BasicNameValuePair("apaterno", str_apaterno));
            params.add(new BasicNameValuePair("amaterno", str_amaterno));
            params.add(new BasicNameValuePair("usuario", str_usuario));
            params.add(new BasicNameValuePair("email", str_email));
            params.add(new BasicNameValuePair("contrasenha", md5(str_contrasenha)));
            params.add(new BasicNameValuePair("pais", str_pais));
            params.add(new BasicNameValuePair("fecha_nacimiento", str_fecha_nacimiento));
            params.add(new BasicNameValuePair("genero", str_genero));
            params.add(new BasicNameValuePair("ruta_foto", ruta_foto));
            JSONObject json = jParser.makeHttpRequest(url_register, "POST", params);
            try {
               int success = json.getInt(TAG_SUCCESS);

               Log.e("Mensaje id:", json.getString("message"));

               if (success == 1) {
                  Integer id_usuario = json.getInt("id");
                  return id_usuario;
               }
            } catch (JSONException e) {
               e.printStackTrace();
            }
         }
         else{
            Log.e("resultado : ", resultado+" ");
            return resultado;
         }
         return 0;
      }
      protected void onPostExecute(Integer id_usuario) {
         // dismiss the dialog after getting all products

         if(id_usuario>0){
            Intent intent = new Intent(getApplicationContext(), LocalizarFamiliarActivity.class);
            intent.putExtra("id_usuario", id_usuario);
            pDialog.dismiss();
            new uploadToServer().execute();
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
         }
         else{
            pDialog.dismiss();
            if(id_usuario==-1) Toast.makeText(RegisterActivity.this, "Ingrese su(s) nombre(s)",Toast.LENGTH_SHORT).show();
            if(id_usuario==-2) Toast.makeText(RegisterActivity.this, "Ingrese su apellido paterno",Toast.LENGTH_SHORT).show();
            if(id_usuario==-3) Toast.makeText(RegisterActivity.this, "Ingrese su apellido materno",Toast.LENGTH_SHORT).show();
            if(id_usuario==-4) Toast.makeText(RegisterActivity.this, "Ingrese nombre de usuario",Toast.LENGTH_SHORT).show();
            if(id_usuario==-5) Toast.makeText(RegisterActivity.this, "Ingrese contraseña, mínimo, 6 caracteres",Toast.LENGTH_SHORT).show();
            if(id_usuario==-6) Toast.makeText(RegisterActivity.this, "Ingrese confirmación de contraseña, mínimo, 6 caracteres",Toast.LENGTH_SHORT).show();
            if(id_usuario==-7) Toast.makeText(RegisterActivity.this, "Las contraseñas no coindicen",Toast.LENGTH_SHORT).show();
            if(id_usuario==-8) Toast.makeText(RegisterActivity.this, "Seleccione nacionalidad",Toast.LENGTH_SHORT).show();
            if(id_usuario==-9) Toast.makeText(RegisterActivity.this, "Ingrese fecha de nacimiento",Toast.LENGTH_SHORT).show();
            if(id_usuario==-10) Toast.makeText(RegisterActivity.this, "Formato de fecha incorrecto (dd-MM-yyyy)",Toast.LENGTH_SHORT).show();
            if(id_usuario==-11) Toast.makeText(RegisterActivity.this, "Seleccione género",Toast.LENGTH_SHORT).show();
            if(id_usuario==-12) Toast.makeText(RegisterActivity.this, "Ingrese su email.",Toast.LENGTH_SHORT).show();
            if(id_usuario==-13) Toast.makeText(RegisterActivity.this, "Formato de email incorrecto",Toast.LENGTH_SHORT).show();
         }
      }
   }


   public class uploadToServer extends AsyncTask<Void, Void, String> {

      private ProgressDialog pd = new ProgressDialog(RegisterActivity.this);

      protected void onPreExecute() {
         super.onPreExecute();
         pd.setMessage("Subiendo foto");
         pd.show();
      }

      @Override
      protected String doInBackground(Void... params) {

         ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
         nameValuePairs.add(new BasicNameValuePair("base64", ba1));
         nameValuePairs.add(new BasicNameValuePair("ImageName", ruta_foto));
         try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(URL);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            String st = EntityUtils.toString(response.getEntity());
            Log.v("log_tag", "In the try Loop" + st);

         } catch (Exception e) {
            Log.v("log_tag", "Error in http connection " + e.toString());
         }
         return "Success";

      }

      protected void onPostExecute(String result) {
         super.onPostExecute(result);
         pd.hide();
         pd.dismiss();
      }
   }


}
