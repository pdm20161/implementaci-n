package pe.edu.uni.pdm_20161.arbolgenealogico;

public class RowItem {

    private String member_name;
    private int profile_pic_id;
    private String lastView;

    public RowItem(String member_name, int profile_pic_id, String lastView) {

        this.member_name = member_name;
        this.profile_pic_id = profile_pic_id;
        this.lastView = lastView;
    }

    public String getMember_name() {
        return member_name;
    }

    public void setMember_name(String member_name) {
        this.member_name = member_name;
    }

    public int getProfile_pic_id() {
        return profile_pic_id;
    }

    public void setProfile_pic_id(int profile_pic_id) {
        this.profile_pic_id = profile_pic_id;
    }

    public String getLastView() {
        return lastView;
    }

    public void setLastView(String contactType) {
        this.lastView = contactType;
    }

}