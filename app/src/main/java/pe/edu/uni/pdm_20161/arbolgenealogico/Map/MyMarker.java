package pe.edu.uni.pdm_20161.arbolgenealogico.Map;

/**
 * Created by m0rb1u5 on 14/02/16.
 */
public class MyMarker {
   private String mLabel;
   private String mIcon;
   private Double mLatitude;
   private Double mLongitude;

   public MyMarker(String mLabel, String mIcon, Double mLatitude, Double mLongitude) {
      this.mIcon = mIcon;
      this.mLabel = mLabel;
      this.mLatitude = mLatitude;
      this.mLongitude = mLongitude;
   }

   public String getmIcon() {
      return mIcon;
   }

   public void setmIcon(String mIcon) {
      this.mIcon = mIcon;
   }

   public String getmLabel() {
      return mLabel;
   }

   public void setmLabel(String mLabel) {
      this.mLabel = mLabel;
   }

   public Double getmLatitude() {
      return mLatitude;
   }

   public void setmLatitude(Double mLatitude) {
      this.mLatitude = mLatitude;
   }

   public Double getmLongitude() {
      return mLongitude;
   }

   public void setmLongitude(Double mLongitude) {
      this.mLongitude = mLongitude;
   }
}
