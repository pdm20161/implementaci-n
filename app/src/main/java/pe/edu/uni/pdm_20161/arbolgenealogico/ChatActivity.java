package pe.edu.uni.pdm_20161.arbolgenealogico;

import github.ankushsachdeva.emojicon.EmojiconEditText;
import github.ankushsachdeva.emojicon.EmojiconGridView.OnEmojiconClickedListener;
import github.ankushsachdeva.emojicon.EmojiconsPopup;
import github.ankushsachdeva.emojicon.EmojiconsPopup.OnEmojiconBackspaceClickedListener;
import github.ankushsachdeva.emojicon.EmojiconsPopup.OnSoftKeyboardOpenCloseListener;
import github.ankushsachdeva.emojicon.emoji.Emojicon;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.sendbird.android.MessageListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdEventHandler;
import com.sendbird.android.model.BroadcastMessage;
import com.sendbird.android.model.Channel;
import com.sendbird.android.model.FileLink;
import com.sendbird.android.model.Message;
import com.sendbird.android.model.MessageModel;
import com.sendbird.android.model.MessagingChannel;
import com.sendbird.android.model.ReadStatus;
import com.sendbird.android.model.SystemMessage;
import com.sendbird.android.model.TypeStatus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ChatActivity extends AppCompatActivity {

   private static final String TAG = MainActivity.class.getSimpleName();
   private MessagesListAdapter adapter;
   private List<MMessage> listMessages;
   private EmojiconEditText emojiconEditText;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_chat);

      initializeEmojis();

      RelativeLayout rLayout = (RelativeLayout) findViewById(R.id.chat_actionbar);
      ActionBar mActionBar = getSupportActionBar();
      //mActionBar.setDisplayShowHomeEnabled(false);
      mActionBar.setDisplayHomeAsUpEnabled(true);
      mActionBar.setDisplayShowTitleEnabled(true);
      mActionBar.setTitle(getIntent().getExtras().getString("pNickname"));
      mActionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.verde_bosque)));
      //LayoutInflater mInflater = LayoutInflater.from(this);
      //View mCustomView = mInflater.inflate(R.layout.chat_actionbar, null);
      //mActionBar.setCustomView(mCustomView);
      mActionBar.setDisplayShowCustomEnabled(true);
      AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
      am.setStreamVolume(AudioManager.STREAM_MUSIC, am.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
      SendBird.startMessaging(getIntent().getExtras().getString("pUserID"));

      SendBird.setEventHandler(new SendBirdEventHandler() {
         MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.notification);

         @Override
         public void onConnect(Channel channel) {

         }

         @Override
         public void onError(int i) {

         }

         @Override
         public void onChannelLeft(Channel channel) {

         }

         @Override
         public void onMessageReceived(com.sendbird.android.model.Message message) {
            if (!message.getMessage().equals("")) {
               if (MainActivity.userID.equals(message.getSenderId())) {
                  MMessage m = new MMessage(getDate(message.getTimestamp()), message.getMessage(), true);
                  appendMessage(m);
               } else {
                  MMessage m = new MMessage(getDate(message.getTimestamp()), message.getMessage(), false);
                  appendMessage(m);
               }
               mp.start();
            }
         }

         @Override
         public void onSystemMessageReceived(SystemMessage systemMessage) {

         }

         @Override
         public void onBroadcastMessageReceived(BroadcastMessage broadcastMessage) {

         }

         @Override
         public void onFileReceived(FileLink fileLink) {

         }

         @Override
         public void onReadReceived(ReadStatus readStatus) {

         }

         @Override
         public void onTypeStartReceived(TypeStatus typeStatus) {

         }

         @Override
         public void onTypeEndReceived(TypeStatus typeStatus) {

         }

         @Override
         public void onAllDataReceived(SendBird.SendBirdDataType sendBirdDataType, int i) {

         }

         @Override
         public void onMessageDelivery(boolean b, String s, String s1, String s2) {

         }

         @Override
         public void onMessagingStarted(final MessagingChannel messagingChannel) {
            SendBird.join(messagingChannel.getUrl());
            SendBird.connect();
            adapter.clear();

            SendBird.queryMessageList(messagingChannel.getUrl()).load(Long.MAX_VALUE, 30, 10, new MessageListQuery.MessageListQueryResult() {
               @Override
               public void onResult(List<MessageModel> messageModels) {
                  /*for (MessageModel model : messageModels) {
                     Object item = model;
                     Message message = (Message) item;
                     if (MainActivity.userID.equals(message.getSenderId())) {
                        MMessage m = new MMessage(getDate(message.getTimestamp()), message.getMessage(), true);
                        appendMessage(m);
                     } else {
                        MMessage m = new MMessage(getDate(message.getTimestamp()), message.getMessage(), false);
                        appendMessage(m);
                     }
                  }*/

                  for (int i = messageModels.size() - 1; i >= 0; --i) {
                     Object item = messageModels.get(i);
                     Message message = (Message) item;
                     if (MainActivity.userID.equals(message.getSenderId())) {
                        MMessage m = new MMessage(getDate(message.getTimestamp()), message.getMessage(), true);
                        appendMessage(m);
                     } else {
                        MMessage m = new MMessage(getDate(message.getTimestamp()), message.getMessage(), false);
                        appendMessage(m);
                     }
                  }
                  //mSendBirdMessagingAdapter.notifyDataSetChanged();
                  //mSendBirdMessagingFragment.mListView.setSelection(30);

                  //SendBird.markAsRead(messagingChannel.getUrl());
                  //SendBird.join(messagingChannel.getUrl());
                  //SendBird.connect(mSendBirdMessagingAdapter.getMaxMessageTimestamp());
               }

               @Override
               public void onError(Exception e) {

               }
            });
         }

         @Override
         public void onMessagingUpdated(MessagingChannel messagingChannel) {

         }

         @Override
         public void onMessagingEnded(MessagingChannel messagingChannel) {

         }

         @Override
         public void onAllMessagingEnded() {

         }

         @Override
         public void onMessagingHidden(MessagingChannel messagingChannel) {

         }

         @Override
         public void onAllMessagingHidden() {

         }
      });

   }

   private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId){
      iconToBeChanged.setImageResource(drawableResourceId);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      //getMenuInflater().inflate(R.menu.menu_localizar_familiar, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      int id = item.getItemId();

      if (id == R.id.action_settings) {
         return true;
      }

      return super.onOptionsItemSelected(item);
   }

   private void appendMessage(final MMessage m) {
      runOnUiThread(new Runnable() {
         @Override
         public void run() {
            listMessages.add(m);

            adapter.notifyDataSetChanged();
         }
      });
   }

   private void initializeEmojis() {

      emojiconEditText = (EmojiconEditText) findViewById(R.id.emojicon_edit_text);
      final ListView lv = (ListView) findViewById(R.id.list_view_messages);
      final ImageView btnSend = (ImageView) findViewById(R.id.submit_btn);
      final View rootView = findViewById(R.id.root_view);
      final ImageView emojiButton = (ImageView) findViewById(R.id.emoji_btn);

      DateFormat df = new SimpleDateFormat("d MMM yyyy HH:mm");
      String date = df.format(Calendar.getInstance().getTime());

      btnSend.setOnClickListener(new View.OnClickListener() {
         DateFormat df = new SimpleDateFormat("d MMM yyyy HH:mm");
         String date;
         @Override
         public void onClick(View v) {
            SendBird.send(emojiconEditText.getText().toString());
            //MMessage m = new MMessage(emojiconEditText.getText().toString(), true);
            //appendMessage(m);
            emojiconEditText.setText("");
         }
      });

      listMessages = new ArrayList<MMessage>();
      adapter = new MessagesListAdapter(this, listMessages);
      lv.setAdapter(adapter);

      //MMessage m = new MMessage("Hola, Qué tal?", false);
      //appendMessage(m);

      // Give the topmost view of your activity layout hierarchy. This will be used to measure soft keyboard height
      final EmojiconsPopup popup = new EmojiconsPopup(rootView, this);

      //Will automatically set size according to the soft keyboard size
      popup.setSizeForSoftKeyboard();

      //If the emoji popup is dismissed, change emojiButton to smiley icon
      popup.setOnDismissListener(new PopupWindow.OnDismissListener() {

         @Override
         public void onDismiss() {
            changeEmojiKeyboardIcon(emojiButton, R.drawable.smiley);
         }
      });

      //If the text keyboard closes, also dismiss the emoji popup
      popup.setOnSoftKeyboardOpenCloseListener(new OnSoftKeyboardOpenCloseListener() {

         @Override
         public void onKeyboardOpen(int keyBoardHeight) {

         }

         @Override
         public void onKeyboardClose() {
            if(popup.isShowing())
               popup.dismiss();
         }
      });

      //On emoji clicked, add it to edittext
      popup.setOnEmojiconClickedListener(new OnEmojiconClickedListener() {

         @Override
         public void onEmojiconClicked(Emojicon emojicon) {
            if (emojiconEditText == null || emojicon == null) {
               return;
            }

            int start = emojiconEditText.getSelectionStart();
            int end = emojiconEditText.getSelectionEnd();
            if (start < 0) {
               emojiconEditText.append(emojicon.getEmoji());
            } else {
               emojiconEditText.getText().replace(Math.min(start, end),
                       Math.max(start, end), emojicon.getEmoji(), 0,
                       emojicon.getEmoji().length());
            }
         }
      });

      //On backspace clicked, emulate the KEYCODE_DEL key event
      popup.setOnEmojiconBackspaceClickedListener(new OnEmojiconBackspaceClickedListener() {

         @Override
         public void onEmojiconBackspaceClicked(View v) {
            KeyEvent event = new KeyEvent(
                    0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
            emojiconEditText.dispatchKeyEvent(event);
         }
      });

      // To toggle between text keyboard and emoji keyboard keyboard(Popup)
      emojiButton.setOnClickListener(new View.OnClickListener() {

         @Override
         public void onClick(View v) {
            //If popup is not showing => emoji keyboard is not visible, we need to show it
            if(!popup.isShowing()){
               //If keyboard is visible, simply show the emoji popup
               if(!popup.isKeyBoardOpen()){
                  popup.showAtBottom();
                  changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
               }

               //else, open the text keyboard first and immediately after that show the emoji popup
               else{
                  emojiconEditText.setFocusableInTouchMode(true);
                  emojiconEditText.requestFocus();
                  popup.showAtBottom();
                  final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                  inputMethodManager.showSoftInput(emojiconEditText, InputMethodManager.SHOW_IMPLICIT);
                  changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
               }
            }

            //If popup is showing, simply dismiss it to show the undelying text keyboard
            else{
               popup.dismiss();
            }
         }
      });
   }

   private String getDate(long timeStamp){

      try{
         DateFormat sdf = new SimpleDateFormat("d MMM yyyy HH:mm");
         Date netDate = (new Date(timeStamp));
         return sdf.format(netDate);
      }
      catch(Exception ex){
         return "xx";
      }
   }

}
