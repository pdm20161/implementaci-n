package pe.edu.uni.pdm_20161.arbolgenealogico;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ChatIntro extends AppCompatActivity {
    private Button entrarChat;
    private EditText txtUserID;
    private EditText txtNickname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_intro);

        txtUserID = (EditText) findViewById(R.id.txtUserID);
        txtNickname = (EditText) findViewById(R.id.txtNickname);
        entrarChat = (Button) findViewById(R.id.btnEntrarChat);
    }

    public void onClickEntrar(View view) {
        MainActivity.userID = txtUserID.getText().toString();
        MainActivity.nickname = txtNickname.getText().toString();
        if (!MainActivity.userID.equals("") && !MainActivity.nickname.equals("")) {
            Intent intent = new Intent(this, SalaChatActivity.class);
            intent.putExtra("AppID", getIntent().getExtras().getString("AppID"));
            intent.putExtra("UserID", MainActivity.userID);
            intent.putExtra("Nickname", MainActivity.nickname);
            startActivity(intent);
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
        }
    }
}
